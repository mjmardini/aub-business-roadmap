import React from 'react';

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = { latestVersion: 'Fetching...', currentVersion: '' };
  }

  async componentDidMount() {
    try {
      let response = await fetch(
        'https://mycorsanywhere.herokuapp.com/https://gitlab.com/mjmardini/aub-business-roadmap/raw/master/package.json'
      );
      let data = await response.json();
      this.setState({ latestVersion: data.version });
      if (
        !localStorage.getItem('version') ||
        localStorage.getItem('version') === 'undefined'
      )
        localStorage.setItem('version', this.state.latestVersion);
      this.setState({ currentVersion: localStorage.getItem('version') });
    } catch {
      console.error("Couldn't fetch version.");
      this.setState({ currentVersion: 'Offline' });
    }
  }

  update() {
    localStorage.setItem('version', this.state.version);
    window.location.reload();
  }

  render() {
    if (this.state.version === 'Offline')
      return (
        <button
          className="w-100"
          onClick={this.update.bind(this)}
          disabled={localStorage.getItem('is-online') === 'false'}
        >
          App offline. Reload Manually.
        </button>
      );
    return this.state.currentVersion === this.state.latestVersion ? (
      <>
        <p>Use the buttons above to navigate.</p>
        <p>
          <strong>Home</strong> automatically checks for updates and informs you
          to press a green button to update.
        </p>
        <p>
          <strong>Course Program</strong> contains all the necessary courses for
          graduation.
        </p>
        <p>
          <strong>Grade Calculator</strong> is used to calculate your grade for
          a specific course during a semester.
        </p>
        <p>
          <strong>Course List</strong> contains all courses as well as links to
          previouses.com per course.
        </p>
        <strong>Version</strong> {this.state.currentVersion}
        <p>
          <a href="https://gitlab.com/mjmardini/aub-business-roadmap">
            Gitlab Source Code
          </a>
        </p>
      </>
    ) : (
      <>
        <h4>New Version Available Update Now!</h4>
        <p>
          Current Version <strong>{this.state.currentVersion}</strong>, Latest
          Version <strong>{this.state.latestVersion}</strong>
        </p>
        <button className="button w-100 add" onClick={this.update.bind(this)}>
          Update!
        </button>
      </>
    );
  }
}

export default Home;
