import React from 'react';
import Quiz from './Quiz'

class GradeCalculator extends React.Component {
	constructor() {
		super();
		this.state = {
			quizes: [],
		}
	}

	calculateGrades() {
		let total = 0;
		this.state.quizes.forEach(quiz => {
			total += quiz.grade * (quiz.percent / 100)
		})
		return total;
	}

	removeQuiz(i) {
		let quiz = this.state.quizes
		quiz.splice(i, 1)
		this.setState({ quizes: quiz, count: this.state.count - 1 })
		this.calculateGrades();
	}

	addQuiz() {
		this.setState({ quizes: [...this.state.quizes, { grade: 0, percent: 0 }], count: this.state.count + 1 })
		this.calculateGrades();
	}

	changeGrade(i, event) {
		event.preventDefault();
		let quiz = this.state.quizes
		quiz.splice(i, 1, { grade: Number(event.target.value), percent: Number(this.state.quizes[i].percent) });
		this.setState({ quizes: quiz })
		this.calculateGrades();
	}

	changePercent(i, event) {
		event.preventDefault();
		let quiz = this.state.quizes
		quiz.splice(i, 1, { grade: Number(this.state.quizes[i].grade), percent: Number(event.target.value) })
		this.setState({ quizes: quiz })
		this.calculateGrades();
	}

	render() {
		return (
			<>
				{this.state.quizes.map((element, i) => {
					return (
						<Quiz grade={element.grade}
							percent={element.percent}
							functions={{ delete: this.removeQuiz.bind(this, i), grade: this.changeGrade.bind(this, i), percent: this.changePercent.bind(this, i) }}
							style={{ width: "100%" }} key={i} />
					)
				})}
				<button className="button add" onClick={this.addQuiz.bind(this)} style={{ width: "100%" }}>Add</button>
				<label className="total">Total: {this.calculateGrades().toFixed(2)}</label>
			</>
		)
	}
}

export default GradeCalculator; 