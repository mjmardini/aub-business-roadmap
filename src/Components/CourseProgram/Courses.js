import React from 'react';


class Courses extends React.Component {
	changeTaken(i) {
		this.props.functions.changeTaken(i, this.props.type);
	}

	changeGrade(i, event) {
		this.props.functions.changeGrade(i, this.props.type, event.target.value);
	}

	render() {
		return (
			<div className="overflow">
				<table className="w-100">
					<thead>
						<tr>
							<th>Course</th>
							<th>Name</th>
							<th>Credits</th>
							<th>Taken</th>
							<th>Grade</th>
							{this.props.type === "core" ? <th>Other Info / Pre-requisite</th> : false}
						</tr>
					</thead>
					<tbody>
						{this.props.data.map((element, i) => {
							return (<tr key={i}>
								<td>{element.id}</td>
								<td>{element.name}</td>
								<td>{element.credits}</td>
								<td><input type="checkbox" className="checkbox" checked={element.taken} onChange={this.changeTaken.bind(this, i)} /></td>
								<td><input type="number" value={element.grade} onChange={this.changeGrade.bind(this, i)} min={0} max={100} readOnly={!element.taken} pattern="[0-9]*" /></td>
								{this.props.type === "core" ? <td>{element.info}</td> : false}
							</tr>)
						})}
					</tbody>
				</table>
			</div>
		);
	}
}

export default Courses