import React from "react";
import { Link } from 'react-router-dom';


function Header(props) {
	return (
		<header>
			<h4 style={{ textAlign: 'center', marginTop: '1em' }}>AUB Business Roadmap</h4>
			<hr />
			<nav className="nav flex">
				<Link to="/" className="button">Home</Link>
				<Link to="/courseprogram" className="button">Courses Program</Link>
				<Link to="/gradecalculator" className="button">Grade Calculator</Link>
				<Link to="/courselist" className="button">Course List</Link>
			</nav>
			<hr />
		</header>
	);
}

export default Header;