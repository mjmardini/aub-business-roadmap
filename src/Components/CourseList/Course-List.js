import React from 'react';
import { Link } from 'react-router-dom';

class CourseList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { courses: [] };
  }

  async componentDidMount() {
    if (localStorage.getItem('course-list')) {
      let dataLocal = JSON.parse(localStorage.getItem('course-list'));
      this.setState({ courses: dataLocal.courses });
    } else {
      try {
        let response = await fetch(
          'https://mycorsanywhere.herokuapp.com/https://jsonbase.com/arp-aub/course-list'
        );
        let data = await response.json();
        this.setState({ courses: data.courses });
        localStorage.setItem('course-list', JSON.stringify(data));
        localStorage.setItem('is-online', 'true');
      } catch (error) {
        console.error(error, 'Server request failed. Server offline.');
        localStorage.setItem('is-online', 'false');
      }
    }
  }

  render() {
    return !this.state.courses.length ? (
      <h3>Loading</h3>
    ) : (
      <table className="w-100">
        <thead>
          <tr>
            <th>Course</th>
            <th>Name</th>
            <th>Credits</th>
          </tr>
        </thead>
        <tbody>
          {this.state.courses.map((element) => {
            return (
              <tr key={element.id}>
                <td>
                  <Link
                    to={`/redirect/AUB-${element.id.replace(
                      /\s+/g,
                      '-'
                    )}-${element.name.replace(/\s+/g, '-')}`}
                  >
                    {element.id}
                  </Link>
                </td>
                <td>{element.name}</td>
                <td>{element.credits}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
}

export default CourseList;
