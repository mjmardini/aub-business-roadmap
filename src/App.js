import React from 'react';
import Header from './Components/Header/Header'
import CourseProgram from './Components/CourseProgram/Course-Program';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import CourseList from './Components/CourseList/Course-List';
import Redirect from './Components/Redirect'
import GradeCalculator from './Components/GradeCalculator/Grade-Calculator';
import Home from './Components/Home/Home';


function App() {
  return (
    <Router basename="aub-business-roadmap/#">
      <div className="container">
        <Header />
        <Switch>
          <Route path="/" exact component={Home}></Route>
          <Route path="/courseprogram" component={CourseProgram} />
          <Route path="/gradecalculator" component={GradeCalculator} />
          <Route path="/courselist" component={CourseList} />
          <Route path="/redirect/:link" component={Redirect}></Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
