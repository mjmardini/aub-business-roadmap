# What is this?

This is a PWA used for business students, currently studying at AUB, as a guide for the business curriculum.

# How do I use this?

Visit [AUB Business Roadmap](https://mjmardini.gitlab.io/aub-business-roadmap). You'll find all courses required by the department as well as prerequisites. As an addition, you can check the courses you've taken to see how what is left for you to take. The app can also suggest courses, take a look at the course list.

# Can I add this to my phone?

Yes, you can. Just click the link above, and add it to Homescreen. Now it'll open as an app like all the others.
